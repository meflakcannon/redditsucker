#The RedditSucker username and password, you can operate this bot without a login, you only need it
#if you want to comment on the post
botlogin=False
username="RedditSucker"
botpassword="BotPassword"

#Subreddits to scan
subreddits=["opendirectories","pics"]

#The file we use to keep track of the posts we have already processes
downloadedIDsFile="/home/ubuntu/redditsucker/downloadedids.txt"

#The path to your Reddit Sucker directory
scriptpath = "/home/ubuntu/redditsucker/"

#Whether or not the bot should comment on the post as it downloads, this is probably not recommended.
comment=False

#Public url of your download directory
publicurl = "http://srv1.opendirectorybot.com/"

#Fetching mode: top, new
fetchmode="new"

#Number of posts in the top/new mode to fetch at one time
getcount=25

#The directory where the bot will download, always use a trailing slash
downloaddirectory="/var/www/html/"

#Descriminate downloads, if you want to download only NSFW or only SFW
discriminateDownloads=False

#Ignore this is you are not discriminating downloads
nonsfw=False
nosfw=False

#The minimum number of upvotes required before the bot will download
minimumUpvotes=0

#Robots-off affects how the bot fetches a URL, it is not recommended to turn this off, it exists for a reason
robotsoff=False

#Depth sets the depth of links to follow
linkdepth=1