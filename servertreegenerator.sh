#!/bin/bash
pushd /var/opendirectorybot/
for D in */
do
	D=${D%"/"}
	tree -F -H "$D" "$D" -o "$D Tree.html"
	echo "Last updated $('date')" >> "$D Tree.html"
done
popd
echo -n "Generated the tree files "
date
