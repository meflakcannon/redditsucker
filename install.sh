#!/bin/bash

which git >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "Git Installed"
        echo "No action needed"
else
        needsgit=1
fi
which apt >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "apt"
        if [ $needsgit -eq 1]
        then
                apt-get install git python-pip
        else
                apt-get install python-pip
        fi
        pip install praw
fi
which yum >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "yum"
        if [ $needsgit -eq 1]
        then
                yum install python-pip.noarch
        else
                yum install python-pip.noarch
        fi
        pip install praw
fi
which pacman >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "pacman"
        if [ $needsgit -eq 1]
        then
                pacman -S  git python-pip
        else
                pacman -S  python-pip
        fi
        pip install praw
fi

echo "RedditSucker  Copyright (C) 2014 Iceberg Technologies Limited"
echo "This program comes with ABSOLUTELY NO WARRANTY"
echo " "
echo "Done installing, copy example-config.py to config.py and update to your liking."
echo " "
echo "Once you're done, Run the script manually with python redditsucker.py or set a cron job!"
